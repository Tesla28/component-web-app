import React, { Fragment, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Navbar from './components/layouts/Navbar';
import Login from './components/layouts/Login';
import Register from './components/register/Register'
import Dashboard from './components/dashboard/Dashboard';
import PrivateRoute from './components/routing/PrivateRoute';
// Redux
import { Provider } from 'react-redux';
import store from './store';
import { loadUser } from './actions/auth';
import setAuthToken from './utils/setAuthToken';

if(localStorage.token){
  setAuthToken(localStorage.token);
}

const App = () => { 
  useEffect(() => {
    store.dispatch(loadUser());
  }, []);

    return (
      <Provider store={store}>
        <Router>
          <Fragment>
            <Navbar />
              <section className="container">
                <Route exact path="/" component={ Login } />
                  <Switch>
                    <Route exact path="/login" component={ Login } />
                    <Route exact path="/register" component={ Register } />
                    <PrivateRoute exact path="/dashboard" component={ Dashboard } />
                  </Switch>
              </section>
          </Fragment>
        </Router>
      </Provider>
    );
};

export default App;
