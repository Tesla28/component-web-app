import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import { setAlert } from './alert';
import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT
} from './types';

// Load User
export const loadUser = () => async dispatch => {

    if(localStorage.token){
        setAuthToken(localStorage.token);
    }

    const config = {
        headers: {
            'token': localStorage.getItem('token'),
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        }
    };

    console.log('config : ' + config.headers);

    try {
        const res = await axios.get('/component-auth/quot/api/auth/getUserFromToken', config);
        
        dispatch({
            type: USER_LOADED,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: AUTH_ERROR
        });
    }
}

// Register user
export const register = ({ name, email, password }) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({ name, email, password });

    try {
        const res = await axios.post('/component-user/quot/api/user/add', body, config);

        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        });

        dispatch(loadUser());
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors){
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type: REGISTER_FAIL
        });
    }
};



// Login user
export const login = ({ userName, password }) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    password = new Buffer(password).toString('base64');

    const body = JSON.stringify({ userName, password });

    try {
        const res = await axios.post('/component-user/quot/api/user/auth', body, config);

        console.log('response : ' + JSON.stringify(res));

        localStorage.setItem('token', res.token);

        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        });

        dispatch(loadUser());
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors){
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type: LOGIN_FAIL
        });
    }
}


// Logout
export const logout = () => dispatch => {
    dispatch({ type: LOGOUT });
}